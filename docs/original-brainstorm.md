# Ludum Dare 45 compo entry

The theme was: Start with nothing.

## Brainstorming notes

* simple RPG - rogue-like dungeon crawler
  * escape prison - can use some concepts from one item, many uses

### Details - story

* You are imprisoned mage. Whole prison gets released at the same time and hell breaks loose.
* You start at empty cell in your underware. Your mana is zero and you have no weapons.

### Details - implemetation

* Top down hack and slash combat game with RPG elements.
* Movement by WSAD but the character turn towards the mouse.
* You regain mana at special place - mana sources.
* You have 4 basic spells - Heal Self, Fireball, Stone Wall, Light
* You can learn more spells from spellbooks - can be looted from other dead mages
* The lower your mana is then your controll becomes more unstable
* You regenerate mana very slowely on your own (every 10 sec 1 mana point)
* You must fight your way out - You start at 3rd level of the prison
  * Lower levels are more filled with enemies

### TODO list

* Add looting and inventory system (very simple - 4 inventory slots and 4 player equipment slots - left and right hand, body, head)
* Add lore story
