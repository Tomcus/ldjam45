# Diary

This document contains information about this games developemnt during #devtober.

## 1. 10. 2021

I have cloned the original repo and tried to run the game with no success. I have tried to revert some changes that were made after the LDJAM to make the game run/compile. I spend 2 hours on this but wasn't able to make the game run (but the number of errors was lower).

## 2. 10. 2021

I fix all the errors and the game was able to start. Since LDJAM 45 I was able to learn more about how Godot engine works, so I started to make some small changes. Also I started to add types to variables.

## 3. 10. 2021

I have finished the basic code overhaul (and added types to most of the funtions). Also moved some documents around to create this diary and to preserve the old one. Also desided on the first feature that I would like to make: Spell serialization. Right now the spells are stored in big dictionary, which is obviously bad, so I will make classes and add support for serializing them. And by that I mean that you can edit them in the editor - in the future :).

## 4. 10. 2021

Started writing this diary. Also made progress on spell system. Created json file structure, that is used for loading spell definitions. But this might change later (aka tomorrow), because it would be easier to create spells directly in editor. So what I think I can do is to create Singleton Node that would store all the spells with their definitions as nodes. So i can preview the spells in editor (both their icon and their effect).

Also I know what I want to work on next. I want to redo the level composition. Every current tile should be split into grid (currently thinking 5\*5, but we will see). Wall should be constructed by 1\*1 wall tiles. This should allow better design. Also I should add door and connect neighbouring walls together and make them react to the spells and also make them destroyable.

## 5. 10. 2021

Finished the first draft of spell overhaul. Some changes needs to be done to how the damage is dealt. But everything works as previously.