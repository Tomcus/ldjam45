# Dev diary

This will be better than creating posts on LD website every time I do something (also I love to write markdown documents) so this should be documentation of my progress.
I started creating this diary on Sat 12:28 CET. All events are can be find in commits (there aren't many) on my gitlab.

## Friday

### Undocumented

Created list of themes and did short brainstoming of ideas of what to do.

## Saturday

### Undocumented

Woke up at 8 and had brakfast. Then read the LD's theme. I created git repo and used README as brainstorming. I created full todo list and list of features that I want to implement. Since I wanted to create 2D top down game I decided to use Godot (for 3D game I would use Unity3D). So I started Godot and started to create basic system layout. I have decided for every interactable item to be Entity derived node (where Enity would be basic node).

The system would look something like this:

* Entity
  * Player
  * Enemy
    * Knight
    * Guard Ghost
  * SpellProjectile
  * Solid (destructable - objects) - this might change because right now these objects can be moved so I might to not have destructable objects or I might cache the position and direction of objects and force them on any change (but that might broke collisions)
    * Wall
    * ...

Then I simply create player movement and spell casting. Also really quickly I implemented the damage dealing

Then I created this diary :D

### 12:50 CET

I created graphics for next feature (the ghost guardian).
I decide to change entity system litle bit and make it more generic

### 13:38 CET

Finished entity system refactor (for now) and implemeted Ghost guardian (no AI yet)

### 14:40 CET

Finished implementing simple AI for Ghost guardian

### 15:10 CET

Did some fixes (especially fixed wall behaviour - player can't move it now)
Also added new Entity type (Static - non movable but destructible)

### 15:20 CET

Lunch :D

### 16:02 CET

Finished camera movement

### 17:00 CET

Don't know how to implement mellee attack - slows me down so I am gonna skip this step for now

### 19:10 CET

Created simple UI - some graphics must be changed
Also started to make preparations for basic level design

### 21:45 CET

Still working on level building tile sets - hopefully this pays off soon

### 22:24 CET

Finally finished the f*cking tiles sets for the level editor - hopefully it works now to test it :).

Please work

### 22:50 CET

Yeah IT works. Now is time for something quick. So I expanded todo list.

### 23:34 CET

Fixed the damn hearth - now looks pretier

## Sunday

### 9:30 CET

Woke up

### 10:00 CET

Made a tea. Ready to continue. 14 hours left. That's enough time to finish this game.

### 11:10 CET

Added spell learning and spell select mechanics also with ui elements

### 11:56 CET

Added main menu

### 13:40 CET

Added first level and level changing mechanics

### Undocumented

From this time onvard I started to panic a little bit. But still I managed to finish everything, that I had planned (mainly - there were few things that didn't make the final version)

## 19.10.2019

Publishing version with fixes (and one new small extra feature - dificulty - that makes the game more accessible).
