# Escape from Nabakza

This game was originaly created during [Ludum Dare](https://ldjam.com/) and continued during [Devtober](https://itch.io/jam/devtober-2021).

Original braninstorming document can be found [here](docs/original-brainstorm.md).

Also both [ldjam](docs/ldjam-diary.md) and [devtober](docs/devtober-diary.md) diaries are stored in this repo.
