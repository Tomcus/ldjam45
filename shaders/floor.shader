shader_type canvas_item;
render_mode unshaded;

uniform vec2 active_position = vec2(0, 0);
uniform int active_distance = 64;

varying vec2 world_pos;

void vertex() {
	world_pos = VERTEX;
}

vec3 avg_color(vec3 color) {
	float avg = 0.0;
	avg += color.r + color.g + color.b;
	avg /= 3.0;
	return vec3(avg, avg, avg);
}

void fragment() {
	float dist = distance(active_position, world_pos);
	vec4 pixel_color = texture(TEXTURE, UV);
	vec4 unshaded_color = vec4(avg_color(pixel_color.rgb), 1.0);
	if (dist > float(active_distance)) {
		COLOR = unshaded_color;
	} else {
		float str = 1.0 - (dist/float(active_distance));
		vec4 diff = pixel_color - unshaded_color;
		COLOR = unshaded_color + (str * diff);
	}
}