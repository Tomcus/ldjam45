extends "res://scripts/Scene.gd"

export (PackedScene) var nextScene = null

var main_menu_scene := load("res://MainMenu.tscn")
var game_scene := preload("res://DifficultySettings.tscn")

func _on_Button_pressed() -> void:
	app.change_scene(main_menu_scene)


func _on_Button2_pressed() -> void:
	app.change_scene(nextScene)


func _on_Button3_pressed() -> void:
	app.change_scene(game_scene)
