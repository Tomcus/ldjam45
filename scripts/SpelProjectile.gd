extends "res://scripts/DamagingEntity.gd"

class_name SpellProjectile

var initial_direction = 1.75*PI

signal on_hit(pos)

func die_to_on_hit() -> void:
	emit_signal("on_hit", get_position())

func _ready() -> void:
	# change_direction(initial_direction)
	var _conn_res := connect("die", self, "die_to_on_hit")

func _process(_delta: float) -> void:
	if is_on_wall() and ($DecayTimer as Timer).is_stopped():
		($DecayTimer as Timer).start()

func _on_DecayTimer_timeout() -> void:
	emit_signal("on_hit", get_position())
	queue_free()
