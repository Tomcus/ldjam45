extends "res://scripts/DamagingEntity.gd"

class_name EnemyEntity

var target: Entity = null

signal spawn_item(position, item)

func rotate_graphics(rotation):
	.rotate_graphics(rotation)
	$Visions.set_rotation(rotation)

func is_valid_target(_body: Entity) -> bool:
	return false

func do_ai() -> void:
	pass

func _on_entity_see(body: Entity) -> void:
	if target == null and is_valid_target(body):
		target = body
		set_rotation(0)

func _ready() -> void:
	for vision in $Visions.get_children():
		vision.connect("body_entered", self, "_on_entity_see")

func _process(_delta: float) -> void:
	if target != null:
		do_ai()

