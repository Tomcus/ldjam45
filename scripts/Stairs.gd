extends Node2D

signal player_entered

func on_player_enter(_player):
	emit_signal("player_entered")

func _ready():
	var _conn_res := $Area2D.connect("body_entered", self, "on_player_enter")
