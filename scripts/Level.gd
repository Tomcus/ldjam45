extends "res://scripts/Scene.gd"

class_name Level

export (PackedScene) var next_level

signal level_finished

func finish_level() -> void:
	emit_signal("level_finished")

func spawn(pos: Vector2, obj: Node, rot: float = NAN) -> void:
	call_deferred("add_child", obj)
	obj.set_position(pos)
	if not is_nan(rot):
		obj.change_direction(rot)

func _ready() -> void:
	var stairs = get_node("Stairs")
	if stairs != null:
		stairs.connect("player_entered", self, "finish_level")
	for enemy in $Entities/Enemies.get_children():
		enemy.connect("spawn_item", self, "spawn")
