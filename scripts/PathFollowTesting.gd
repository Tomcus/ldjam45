extends Node2D

export var speed = 80
export var max_offset = 0

signal route_started
signal route_finished

var offset = 0
var is_running = true

func start():
	is_running= true
	offset = 0
	emit_signal("route_started")

func _process(delta):
	if is_running:
		offset += speed * delta
		$Path2D/PathFollow2D.set_offset(offset)
		# print($Path2D/PathFollow2D.get_offset())
		if offset > max_offset:
			is_running = false
			emit_signal("route_finished")
