extends "res://scripts/Enemy.gd"

class_name Assassin

const SHORT_DISTANCE := 50

var attack_timeout := 0.0

func is_valid_target(_body: Entity) -> bool:
	if _body.maxMana > 0:
		return true
	return false

func _process(delta: float) -> void:
	attack_timeout -= delta

func do_ai() -> void:
	change_direction((target.get_position() - get_position()).angle())
	var distance_to_target := get_position().distance_to(target.get_position())
	if distance_to_target > 64:
		speed = 150
	elif distance_to_target > SHORT_DISTANCE - 25:
		speed = 75
	else:
		speed = -100
	if attack_timeout < 0:
		if distance_to_target < SHORT_DISTANCE:
			do_attack($MelleeAttacks/SpearStab)
			attack_timeout = 0.7
