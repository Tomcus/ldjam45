extends Node2D

var spells := {}

func get_spell(spell_name: String):
	if spell_name in spells:
		return spells[spell_name]
	return null

func _ready():
	for spell in get_children():
		spells[spell.spell_name] = spell
