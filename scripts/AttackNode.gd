extends Node2D

class_name AttackNode

var spell_projectile := load("res://SpellProjectile.tscn")

export (Texture) var attack_sprite = null
export var attack_damage := 0
export var speed := 0
export var max_offset := 0

signal attack_finished

var offset := 0
var is_running := false

func start():
	offset = 0
	is_running = true
	var proj = spell_projectile.instance()
	$Path2D/PathFollow2D.add_child(proj)
	proj.damage = attack_damage
	proj.speed = 0
	proj.get_node("Sprite").texture = attack_sprite
	proj.author = get_parent().get_parent()

func _process(delta):
	if is_running:
		offset += delta * speed
		$Path2D/PathFollow2D.set_offset(offset)
		if offset > max_offset:
			is_running = false
			emit_signal("attack_finished")
			for attacker in $Path2D/PathFollow2D.get_children():
				attacker.queue_free()
