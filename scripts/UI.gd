extends CanvasLayer

export (PackedScene) var spell_icon = null

func set_health(new_health):
	$BottomDownBars/Health/Numb.text = str(new_health)
	
func set_mana(new_mana):
	$BottomDownBars/Mana/Numb.text = str(new_mana)

func update_spell_list(spells):
	if spell_icon != null:
		for child in $BottomDownBars/Spells.get_children():
			child.queue_free()
		var index = 1
		for spell in spells:
			var icon = spell.icon
			var si = spell_icon.instance()
			$BottomDownBars/Spells.add_child(si)
			si.set_texture(icon)
			si.get_child(0).text = str(index)
			index += 1
