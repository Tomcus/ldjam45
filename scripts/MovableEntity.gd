extends "res://scripts/Entity.gd"

class_name MovableEntity

const RIGHT := Vector2(1, 0)

export var direction := Vector2(0, 0);
export var speed := 0;

var ma_alternatives := 0
var current_alternative := 0
var can_attack := true

func reset_attack() -> void:
	can_attack = true

func do_attack(attack: AttackNode) -> void:
	if can_attack:
		can_attack = false
		attack.start()

func do_mellee_attack() -> void:
	if ma_alternatives > 0 and can_attack:
		var current_attack := $MelleeAttacks.get_child(current_alternative)
		do_attack(current_attack)
		current_alternative += 1
		current_alternative %= ma_alternatives

func _ready() -> void:
	ma_alternatives = $MelleeAttacks.get_child_count()
	for mellee in $MelleeAttacks.get_children():
		mellee.connect("attack_finished", self, "reset_attack")

func change_direction(_rotation: float) -> void:
	rotate_graphics(_rotation)
	direction = RIGHT.rotated(_rotation)

func rotate_graphics(rotation: float) -> void:
	$Sprite.set_rotation(rotation)
	$CollisionPolygon2D.set_rotation(rotation)
	$MelleeAttacks.set_rotation(rotation)
	
func move_with_direction() -> void:
	if (stunt < 0):
		var _ms_res := move_and_slide(direction * speed * app.current_settings["entity_speed"])

func _process(_delta: float) -> void:
	move_with_direction()
