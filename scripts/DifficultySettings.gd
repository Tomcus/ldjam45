extends "res://scripts/Scene.gd"

var game_scene := preload("res://Game.tscn")

func _on_HardButton_pressed() -> void:
	app.current_settings = app.SETTINGS["HARD"]
	app.change_scene(game_scene)


func _on_EasyButton_pressed() -> void:
	app.current_settings = app.SETTINGS["EASY"]
	app.change_scene(game_scene)


func _on_NormalButton_pressed() -> void:
	app.current_settings = app.SETTINGS["NORMAL"]
	app.change_scene(game_scene)
