extends "res://scripts/Enemy.gd"

export (PackedScene) var orb = null

func is_valid_target(body):
	if body.is_alive:
		return true
	return false

func do_ai():
	if target.health > 0:
		change_direction((target.get_position() - get_position()).angle())
	else:
		target = null

func _ready():
	var _conn_res := connect("die", self, "spawn_orb")

func spawn_orb():
	var orb_mana := int(5 + rand_range(-2, 2))
	var orb_health := int(0 + rand_range(0, 2))
	var new_orb = orb.instance()
	new_orb.mana_bonus = orb_mana
	new_orb.health_bonus = orb_health
	emit_signal("spawn_item", position, new_orb)
