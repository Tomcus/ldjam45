extends Node2D

class_name AppCore

export (PackedScene) var mainScene = null

# signal song_queue_empty

const SETTINGS := {
	"NORMAL": {
		"entity_speed": 1
	},
	"EASY": {
		"entity_speed": 0.75
	},
	"HARD": {
		"entity_speed": 1.5
	}
}

var song_queue := []
var current_settings: Dictionary = SETTINGS["NORMAL"]

func quit_app() -> void:
	get_tree().quit()

func change_scene(scene: PackedScene) -> void:
	for loadedScene in $Scene.get_children():
		loadedScene.queue_free()
	var new_scene = scene.instance()
	$Scene.add_child(new_scene)

func _ready() -> void:
	change_scene(mainScene)
