extends "res://scripts/Scene.gd"

export (PackedScene) var current_level = null
var level_index = 0

func spawn_level(level: PackedScene) -> void:
	current_level = level.instance()
	call_deferred("add_child", current_level)
	call_deferred("current_level", 0)
	var _conn_res = $Player.connect("castspell", current_level, "spawn")
	var player_pos = current_level.get_node("PlayerPos")
	$Player.transform = player_pos.transform
	current_level.connect("level_finished", self, "next_level")

func show_death_screen() -> void:
	var death_scene := preload("res://DeathScene.tscn")
	app.change_scene(death_scene)

func next_level() -> void:
	var level = current_level.next_level
	if current_level != null:
		$Player.disconnect("castspell", current_level, "spawn")
		current_level.queue_free()
	if level != null:
		spawn_level(level)
	else:
		var win_scene = preload("res://WinScene.tscn")
		app.change_scene(win_scene)

func _ready():
	if not $Player.is_connected("die", self, "show_death_screen"):
		var _conn_res := $Player.connect("die", self, "show_death_screen")
	spawn_level(current_level)
