extends Node2D

class_name SpellBase

class Damage:
	export var health := 0.0
	export var mana := 0.0
	export var stun := -1.0

export var spell_name: String = "Unknown"
export var icon: Texture = null
export var projectile_image: Texture = null
export var projectile_angles: PoolRealArray = [0]
export var health_damage: float = 0.0
export var mana_damage: float = 0.0
export var stun_damage: float = -1.0
export var health_cost: float = 0.0
export var mana_cost: float = 0.0
export var stun_cost: float = -1.0
export var speed: int = 0
export var on_hit: String = ""

func get_damage() -> Damage:
	var res := Damage.new()
	res.health = health_damage
	res.mana = mana_damage
	res.stun = stun_damage
	return res

func get_cost() -> Damage:
	var res := Damage.new()
	res.health = health_cost
	res.mana = mana_cost
	res.stun = stun_cost
	return res
