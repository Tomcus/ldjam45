extends "res://scripts/MovableEntity.gd"

signal castspell(world_pos, spell, spell_rotation)

export (PackedScene) var spell_projectile = null

var spell_cast_space := 16
var known_spells := []
var current_spell = null

onready var mana_regen_timer: Timer = $ManaRegen

func _ready() -> void:
	var _timeout_conn_res := mana_regen_timer.connect("timeout", self, "mana_regen")
	reset_mana_regen()
	var _body_entered_conn_res := $SpellLearner.connect("body_entered", self, "learn_spell")
	
func reset_mana_regen() -> void:
	mana_regen_timer.wait_time = get_mana_regen_timeout()
	mana_regen_timer.start()

func change_direction(_rotation: float) -> void:
	var mouse_position = get_global_mouse_position()
	var player_position = get_position()
	rotation = player_position.angle_to_point(mouse_position) - PI
	rotate_graphics(rotation)
	
func mana_regen() -> void:
	if mana < maxMana:
		mana += 1 # TODO: calculate based on equipment
	reset_mana_regen()

func get_mana_regen_timeout() -> float:
	return 5.0 # TODO: calculate based on equipment

func process_move_input() -> Vector2:
	var v := Vector2(0, 0)
	if Input.is_action_pressed("ui_up"):
		v.y -= 1
	if Input.is_action_pressed("ui_down"):
		v.y += 1
	if Input.is_action_pressed("ui_left"):
		v.x -= 1
	if Input.is_action_pressed("ui_right"):
		v.x += 1
	v = v.normalized()
	return v

func move() -> void:
	var v := process_move_input()
	direction = v

func change_spell() -> void:
	var index := -1
	if Input.is_action_just_pressed("spell_1"):
		index = 0
	elif Input.is_action_just_pressed("spell_2"):
		index = 1
	elif Input.is_action_just_pressed("spell_3"):
		index = 2
	elif Input.is_action_just_pressed("spell_4"):
		index = 3
	elif Input.is_action_just_pressed("spell_5"):
		index = 4
	elif Input.is_action_just_pressed("spell_6"):
		index = 5
	if index >= 0 and index < known_spells.size():
		current_spell = known_spells[index]

func learn_spell(spell: SpellTome) -> void:
	print("Learning spell " + spell.spell_name)
	var spell_data = Spells.get_spell(spell.spell_name)
	if spell_data != null:
		known_spells.append(spell_data)
	spell.queue_free()

func spawn_megashot_2(pos: Vector2) -> void:
	var mega_shot_2 = Spells.get_spell("mega_shot_2")
	create_projectile(mega_shot_2, pos)

func create_projectile(spell, pos: Vector2, rot: float = 0.0) -> void:
	for i in spell.projectile_angles:
		var new_projectile := spell_projectile.instance() as SpellProjectile
		emit_signal("castspell", pos, new_projectile, rot + i)
		new_projectile.author = self
		if spell.on_hit != "":
			new_projectile.connect("on_hit", self, spell.on_hit)
		new_projectile.get_node("Sprite").set_texture(spell.projectile_image)
		new_projectile.speed = spell.speed
		new_projectile.damage = spell.health_damage
		new_projectile.mana_damage = spell.mana_damage
		new_projectile.stun_damage = spell.stun_damage

func can_cast(spell_cost) -> bool:
	return mana > spell_cost.mana and health > spell_cost.health

func cast_spell(projectile_rotation: float) -> bool:
	# projectile_rotation = 2*PI - projectile_rotation
	if Input.is_action_just_pressed("spell_cast") and current_spell != null and can_cast(current_spell.get_cost()):
		if current_spell.projectile_image != null:
			var new_pos = get_position() + Vector2(cos(projectile_rotation), sin(projectile_rotation)) * spell_cast_space
			create_projectile(current_spell, new_pos, projectile_rotation)
		var cost = current_spell.get_cost()
		health -= cost.health
		mana -= cost.mana
		#if cost.stun >= 0.0:
		#	stun += cost.stun
		return true
	return false

func _update_ui() -> void:
	$UI.set_health(health)
	$UI.set_mana(mana)
	$UI.update_spell_list(known_spells)

func do_mellee() -> void:
	if Input.is_action_just_pressed("mellee_attack"):
		do_mellee_attack()

func _process(_delta: float) -> void:
	check_alive()
	if stunt < 0.0:
		change_direction(0.0)
		move()
		change_spell()
		if not cast_spell(rotation):
			do_mellee()
	_update_ui()
