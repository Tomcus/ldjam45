extends Sprite

export var mana_bonus := 5
export var health_bonus := 0


func _on_Area2D_body_entered(body: Entity) -> void:
	body.health += health_bonus
	body.mana += mana_bonus
	queue_free()

