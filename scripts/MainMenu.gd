extends "res://scripts/Scene.gd"

func _on_Exit_pressed() -> void:
	get_tree().quit()

func _on_Play_pressed() -> void:
	var scene = preload("res://Storyboard.tscn")
	app.change_scene(scene)
