extends Node2D

onready var shader = preload("res://shaders/floor.shader");

func _ready():
	$FloorSprite.material = ShaderMaterial.new();
	$FloorSprite.material.shader = shader;

func get_position():
	var pos = get_global_mouse_position() - position;
	return pos;

func _process(_delta: float):
	var pos = get_position();
	$FloorSprite.material.set_shader_param("active_position", pos);
