extends KinematicBody2D

class_name Entity

export var maxHealth := 0.0
export var maxMana := 0.0

export var is_alive: bool = false
export var is_player: bool = false

var health := 0.0
var stunt := -1.0
var mana := 0.0
var armor := 0.0

onready var app := get_node("/root/AppCore")

signal die

func validate_health() -> void:
	health = min(health, maxHealth)

func check_alive() -> void:
	if health <= 0:
		emit_signal("die")
		if not is_player:
			queue_free()

func get_armor_reduced_damage(damage: float) -> float:
	if damage > 0:
		return max(0.0, damage - armor)
	return damage

func deal_damage(h_damage: float, m_damage: float = 0, s_damage: float = -1) -> void:
	health -= get_armor_reduced_damage(h_damage)
	validate_health()
	check_alive()
	mana = max(0, mana - m_damage)
	stunt = s_damage

func _ready() -> void:
	health = maxHealth

func _process(delta: float) -> void:
	stunt -= delta
