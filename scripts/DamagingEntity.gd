extends "res://scripts/MovableEntity.gd"

class_name DamagingEntity

export var damage := 0.0
export var mana_damage := 0.0
export var stun_damage := 0.0
export var damage_start_timeout := 0.0
export var on_hit_damage := 0.0

var is_damaging := false

var author: Node2D = null

func _on_hit(body: Node2D) -> void:
	var p_name := body.get_parent().get_name()
	if body != self and author != body and is_damaging and p_name != "Damagers" and p_name != "Visions":
		(body as Entity).deal_damage(damage, mana_damage, stun_damage)
		deal_damage(int(on_hit_damage))

func rotate_graphics(rotation: float) -> void:
	.rotate_graphics(rotation)
	$Damagers.set_rotation(rotation)

func be_damaging() -> void:
	is_damaging = true

func _ready() -> void:
	if damage_start_timeout > 0:
		pass
	else:
		is_damaging = true
	for damager in $Damagers.get_children():
		damager.connect("body_entered", self, "_on_hit")

