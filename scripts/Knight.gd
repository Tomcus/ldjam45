extends "res://scripts/Enemy.gd"

class_name Knight

const SHORT_DISTANCE = 35

var attack_timeout := 0.0

func is_valid_target(body: Entity) -> bool:
	if body.maxMana > 0:
		return true
	return false

func _process(delta: float) -> void:
	attack_timeout -= delta

func do_ai() -> void:
	change_direction((target.get_position() - get_position()).angle())
	var distance_to_target := get_position().distance_to(target.get_position())
	if distance_to_target > 64:
		speed = 120
	elif distance_to_target > SHORT_DISTANCE:
		speed = 75
	elif distance_to_target > 25:
		speed = 0
	else:
		speed = -40
	if attack_timeout < 0:
		if distance_to_target < SHORT_DISTANCE-5:
			do_attack($MelleeAttacks/ShieldBash)
			attack_timeout = 0.2
		elif distance_to_target < SHORT_DISTANCE:
			if randi()%2 == 1:
				do_attack($MelleeAttacks/SidewaysStab)
				attack_timeout = 0.5
			else:
				do_attack($MelleeAttacks/OverShieldStab)
				attack_timeout = 0.4
